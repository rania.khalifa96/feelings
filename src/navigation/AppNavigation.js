/* eslint-disable prettier/prettier */

import React from 'react';
import Load from '../core/onboarding/load/Load';
import Login from '../core/onboarding/login/Login';
import HomeScreen from '../core/mainScreens/HomeScreen/HomeScreen'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

const Stack = createStackNavigator();

function LoginStack() {
  return (
    <Stack.Navigator
     screenOptions={{
      headerShown: false}}>
      <Stack.Screen
      name="load"
      component={Load}/>
      <Stack.Screen
      name="login"
      component={Login}/>
    </Stack.Navigator>
  );
}

    function AppNavigation() {
      return (
        <NavigationContainer>
        <Stack.Navigator   screenOptions={{
          headerShown: false
          }}
          backBehavior='never'>
          <Stack.Screen name="LoginStack" component={LoginStack}/>
          <Stack.Screen name="home" component={HomeScreen}/>
        </Stack.Navigator>
        </NavigationContainer>
      );
    }

export default AppNavigation;
