/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React , {useState, useEffect}from 'react';
import {
  Alert,
  ActivityIndicator
} from 'react-native';
import {Item , Form, Label , Input , Container, Content, Button, Text } from 'native-base';
import dynamicStyles from './Styles';
import * as firebase from "firebase";

const styles = dynamicStyles()

const Login = (props) => {
   
  const {navigation} =props;
  const [email,setEmail] = useState(null);
  const [name,setName] = useState(null);
  const [password,setPassword] = useState(null);
  const [result, setResult]= useState(null);
  const [spinner, setSpinner]= useState(null);
  
   const log =async() =>{
     setSpinner(true)
     try{
    await firebase.auth().createUserWithEmailAndPassword(email, password)
    navigation.navigate('home',{name})
    setSpinner(null)
    }catch(error) {
      if(error.code==='auth/email-already-in-use')
      {
      setResult('change email address')
      setSpinner(null)
      }
      else if(error.code==='auth/argument-error'){
      setResult('Email and password are required')
      setSpinner(null)
      }
      else{
      // Handle Errors here.
      setResult(error.message)
      setSpinner(null)
    }
    };
  }

    return(
      <Container style={styles.container}>
        <Content>
        {/* <Image style={styles.imageStyle} source={require('../../../assets/rainbow.jpg')}/> */}
        <Text style={styles.title}>Register</Text>
        <Form style={styles.form}>
            <Item stackedLabel last>
              <Label  style={{color:'#CC8443'}}>Name</Label>
              <Input value={name} onChangeText={(e) => setName(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label  style={{color:'#CC8443'}}>Email</Label>
              <Input value={email} onChangeText={(e) => setEmail(e)}/>
            </Item>
            <Item stackedLabel last>
              <Label style={{color:'#CC8443'}}>passowrd</Label>
              <Input secureTextEntry={true} value={password} onChangeText={(e) => setPassword(e)}/>
            </Item>
          </Form>
           <Text style={styles.warning}> {result}</Text>
           {spinner!=null ?(
             <ActivityIndicator style={{alignContent:'center'}} color='black' size="large"/>
           ):null}
          <Button
          rounded
          block
          large
          style={styles.buttonStyle}
           onPress={()=>{log()}}
          >
              <Text style={styles.text}> LogIn </Text>
              </Button>
          </Content>
      </Container>
    );
}
export default Login;
