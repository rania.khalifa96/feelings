/* eslint-disable prettier/prettier */
import { StyleSheet , Dimensions} from 'react-native';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const dynamicStyles = () => {
  return new StyleSheet.create({
    buttonStyle:{
        backgroundColor:'#DD904A', 
        marginTop:90,
        alignSelf:'center',
    },
    form:{
      marginTop:100,
    },
    title:{
      marginTop:50,
      fontSize:60,
      fontWeight:'bold',
      color : '#DD904A',
      alignSelf:'center',
      textAlign:'center'
    },
    warning:{
      marginTop:10,
      fontSize:15,
      fontWeight:'bold',
      color : 'red',
      alignSelf:'center'
    },
    text:{
        fontSize:25,
      fontWeight:'bold',
      color:'black'
    },
    imageStyle:{
      flex:1,
      alignSelf:'center',
      height:windowWidth/2,
      width:windowWidth/2,
      marginTop:20
    },
})}
export default dynamicStyles;