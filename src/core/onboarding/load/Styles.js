/* eslint-disable prettier/prettier */
import { StyleSheet , Dimensions} from 'react-native';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const dynamicStyles = () => {
  return new StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
    },
    buttonStyle:{
        flex:1,
        width:'40%',
        backgroundColor:'#DD904A', 
        marginTop:200,
        alignSelf:'center'
    },
    imageStyle:{
      height:windowHeight,
      width:windowWidth,
      position:'relative'
    },
    title:{
      marginTop:50,
      marginLeft:6,
      fontSize:70,
      fontWeight:'bold',
      color : '#DD904A',
    },
    text:{
        fontSize:25,
      fontWeight:'bold',
      color:'black'
    }
})}
export default dynamicStyles;