/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React from 'react';
import {
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';

import { Container, Content, Button, Text} from 'native-base';
import dynamicStyles from './Styles';

const styles = dynamicStyles()
const welcome = (props) => {
    const {navigation} = props;
   const start = () =>{navigation.navigate('login')}

   return(
    <Container style={styles.container}>
      <Content>
        <Text style={styles.title}>Welcome to feelings App</Text>
        <Button 
        rounded 
        large
        style={styles.buttonStyle}
        onPress={start}   
        >
            <Text style={styles.text}> Enter </Text>
            </Button>
        </Content>
    </Container>
  );
}

export default welcome;
