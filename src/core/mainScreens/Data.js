/* eslint-disable prettier/prettier */
const DATA=[
    {
       id: 1,
       feeling: 'Great',
       icon:'emoticon-excited'
     },
     {
       id: 2,
       feeling: 'Good',
       icon:'emoticon-tongue'
     },
     {
       id: 3,
       feeling: 'Alright',
       icon:'emoticon-happy'
     },
     {
       id: 4,
       feeling: 'A bit down',
       icon:'emoticon-sad'
     },
     {
       id: 5,
       feeling: 'Terrible',
       icon:'emoticon-cry'
     },
 ]
export default DATA;