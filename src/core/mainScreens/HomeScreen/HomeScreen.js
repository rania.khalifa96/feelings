/* eslint-disable semi */
/* eslint-disable keyword-spacing */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */

import React,{useState, useEffect} from 'react';
import {
  Alert,
  Text,
  FlatList,
  TouchableOpacity
} from 'react-native';
import * as firebase from "firebase";
import { Container, Content, Button,Icon, Header,Left,Body,Right} from 'native-base';
import dynamicStyles from './Styles';
import DATA from '../Data';

const styles = dynamicStyles()


const Home = (props) => {
  
    const {navigation} = props;
    const [selectedId, setSelectedId] = useState(null);
    const [result, setResult] = useState(null);
    const name =props.route.params.name;

    const logData =(item)=>{
    var date = new Date()
    var userId = firebase.auth().currentUser.uid;
      try{
         firebase.database().ref('/users/' + userId)
       .push().set({
       item:item.feeling,
       time:date
    })
    setResult('You felt '+item.feeling+' at '+date)
    }catch(e){
      Alert.alert(e.message)
    }
    }

    const Item = ({ item, onPress, style }) => (
      <TouchableOpacity onPress={onPress} style={styles.TouchableStyle}>
       <Icon type="MaterialCommunityIcons" style={styles.IconStyle} name={item.icon} />
       <Text style={{marginLeft:8}}>{item.feeling} </Text>
      </TouchableOpacity>
    );
   const renderItem = ({ item }) => {
    return (
      <Item
        item={item}
        onPress={() => {logData(item)} }
      />
    );
  };
   return(
    <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent style={{flex:1}}>
              <Icon type="FontAwesome" style={{ color: 'black'}} name='arrow-left' onPress={() => navigation.goBack()} />
            </Button>
            </Left>
            <Body />
            <Right/>
        </Header>
      <Content>
        <Text style={styles.text}>Good to meet you {name}</Text>
        <Text style={styles.title}>How are you feeling today?</Text>
        <Text style={styles.seText}>This will help me understand you better</Text>
        <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item._id}
        extraData={selectedId}
      />
      <Text style={styles.result}>{result}</Text>
        </Content>
    </Container>
  );
}

export default Home;
