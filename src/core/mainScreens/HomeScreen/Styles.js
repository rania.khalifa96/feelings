/* eslint-disable prettier/prettier */
import { StyleSheet , Dimensions} from 'react-native';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;
const dynamicStyles = () => {
  return new StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
    },
    header:{
      backgroundColor:'#FAFAFA'
    },
    TouchableStyle:{
      flexDirection: "row",
       marginTop:24 
    },
    IconStyle:{ 
      color: '#EACA00',
      marginLeft:8
    },
    buttonStyle:{
        flex:1,
        width:'40%',
        backgroundColor:'#CFC952', 
        marginTop:80,
        left:windowWidth/3.4
    },
    imageStyle:{
      flex:1,
      marginTop:windowHeight/6,
      height:windowWidth,
      width:windowWidth
    },
    title:{
      marginLeft:5,
      fontSize:30,
      color : 'black',
    },
    seText:{
      marginLeft:5,
      marginTop:20,
      marginBottom:40,
      fontSize:15,
      color:'black'
    },
    text:{
      marginLeft:5,
      marginTop:30,
      marginBottom:20,
      fontSize:15,
      color:'black'
    }, 
    result:{
      marginLeft:5,
      marginTop:60,
      marginBottom:20,
      fontSize:15,
      color:'#CFB200'
    }
})}
export default dynamicStyles;