/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React ,{useEffect} from 'react';
import {
  StatusBar,
} from 'react-native';
import AppNavigation from './src/navigation/AppNavigation';
import * as firebase from "firebase";

const App =() => {
  useEffect(() => {
    async function fetchData() {
      try {
        await firebase.initializeApp({
          apiKey: "AIzaSyDb7r9HJ-5PauHdjkezE-rg1yDdofc-EAI",
          authDomain: "feelings-5f45c.firebaseapp.com",
          databaseURL: "https://feelings-5f45c.firebaseio.com",
          projectId: "feelings-5f45c",
          storageBucket: "feelings-5f45c.appspot.com",
          messagingSenderId: "37166893531",
          appId: "1:37166893531:web:c9f40f31368c421ca57ffe",
          measurementId: "G-CNJWF8TVB5"
        });} catch (err) {
          // we skip the "already exists" message which is
          // not an actual error when we're hot-reloading
          if (!/already exists/.test(err.message)) {
          console.error('Firebase initialization error', err.stack)
          }
          }
    }
    fetchData();
  }, []);
  return (
    <>
      <StatusBar barStyle="dark-content" />
           <AppNavigation />
    </>
  );
};


export default App;
